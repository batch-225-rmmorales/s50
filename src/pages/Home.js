// import "./App.css";
// import "./App.css";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Container, Row, Spinner } from "react-bootstrap";
import CourseCard from "../components/CourseCard";
import coursesData from "../data/CoursesData";
import { useEffect, useState } from "react";

function Home() {
  // Set loading state to true initially
  let [loading, setLoading] = useState(true);
  useEffect(() => {
    // Loading function to load data or
    // fake it using setTimeout;
    const loadData = async () => {
      // Wait for two second
      console.log("im in load data");

      await new Promise((r) => setTimeout(r, 500));

      // Toggle loading state
      if (loading) {
        setLoading(false);
        console.log(loading);
      }
    };

    loadData();
  });

  if (loading) {
    return (
      <Row className="justify-content-center align-items-center vh-75">
        <Spinner animation="border" role="status" size="lg">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Row>
    );
  }

  // If page is not in loading state, display page.
  else {
    console.log(loading);
    return (
      <>
        <Banner />
        <Highlights />
      </>
    );
  }
}

export default Home;
