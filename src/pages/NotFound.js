import { Form, Button, Col, Row } from "react-bootstrap";
import { useEffect, useState } from "react";

export default function NotFound() {
  return (
    <div class="row justify-content-center align-items-center vh-100">
      <div className="col-auto">
        <table>
          <tr>
            <td className="text-center">
              <h1>📝</h1>
            </td>
            <td className="text-center">
              <h1>❌</h1>
            </td>
            <td className="text-center">
              <h1>🔎</h1>
            </td>
          </tr>
          <tr>
            <td className="text-center">
              <h1>&#40;Page&#41;</h1>
            </td>
            <td className="text-center">
              <h1>&#40;Not&#41;</h1>
            </td>
            <td className="text-center">
              <h1>&#40;Found&#41;</h1>
            </td>
          </tr>
        </table>

        <p className="text-center">
          Go back to <a href="/">homepage</a>
        </p>
      </div>
    </div>
  );
}
