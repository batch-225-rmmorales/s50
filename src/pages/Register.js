import { Form, Button, Col, Row } from "react-bootstrap";
import { useEffect, useState } from "react";

export default function Register() {
  // State hooks
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  useEffect(() => {
    if (password1 === password2 && email !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    console.log(isActive);
    // document.querySelector("#submitBtn").setAttribute("disabled", !isActive);
    console.log(document.querySelector("#submitBtn"));
  }, [email, password1, password2, isActive]);

  const registerUser = (event) => {
    event.preventDefault();
    setEmail("");
    setPassword1("");
    setPassword2("");
    alert("user is registered");
  };
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
  };
  const onChangePassword1 = (event) => {
    setPassword1(event.target.value);
  };
  const onChangePassword2 = (event) => {
    setPassword2(event.target.value);
  };
  return (
    <Col xs={12} md={4}>
      <h1>Register</h1>
      <Form onSubmit={registerUser}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            onChange={onChangeEmail}
            value={email}
            type="email"
            placeholder="Enter email"
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            onChange={onChangePassword1}
            value={password1}
            type="password"
            placeholder="Password"
            required
          />
        </Form.Group>

        <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            onChange={onChangePassword2}
            value={password2}
            type="password"
            placeholder="Verify Password"
            required
          />
        </Form.Group>
        {/* pde rin pala ung code blocks tapos naka {isActive ? <HTML>: </HTML>} */}
        <Button
          variant="primary"
          type="submit"
          id="submitBtn"
          disabled={!isActive}
        >
          Submit
        </Button>
      </Form>
    </Col>
  );
}
