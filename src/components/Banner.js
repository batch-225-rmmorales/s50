// import Button from "react-boostrap/Button";
// import Row from "react-boostrap/Row";
// import Col from "react-boostrap/Col";

import { Button, Row, Col } from "react-bootstrap";

export default function Banner() {
  return (
    <Row>
      <Col>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere.</p>
        <Button variant="primary">Enroll now!</Button>
      </Col>
    </Row>
  );
}
