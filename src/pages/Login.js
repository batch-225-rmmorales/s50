import { Form, Button, Col, Row } from "react-bootstrap";
import { useEffect, useState } from "react";

export default function Login({ isLoggedIn, setIsLoggedIn }) {
  // State hooks
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [isActive, setIsActive] = useState(false);
  useEffect(() => {
    if (password1 !== "" && email !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    console.log(isActive);
    // document.querySelector("#submitBtn").setAttribute("disabled", !isActive);
    console.log(document.querySelector("#submitBtn"));
  }, [email, password1, isActive]);

  const signin = (event) => {
    event.preventDefault();

    localStorage.setItem("jwtToken", 1);
    setIsLoggedIn(true);

    setEmail("");
    setPassword1("");
    window.location.href = "/";
  };
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
  };
  const onChangePassword1 = (event) => {
    setPassword1(event.target.value);
  };

  return (
    <Col xs={12} md={4}>
      <h1>Login</h1>
      <Form onSubmit={signin}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            onChange={onChangeEmail}
            value={email}
            type="email"
            placeholder="Enter email"
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            onChange={onChangePassword1}
            value={password1}
            type="password"
            placeholder="Password"
            required
          />
        </Form.Group>
        <Button
          variant="primary"
          type="submit"
          id="submitBtn"
          disabled={!isActive}
        >
          Login
        </Button>
      </Form>
    </Col>
  );
}
