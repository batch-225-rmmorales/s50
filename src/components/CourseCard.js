import { Button, Row, Col, Card } from "react-bootstrap";
import { useState, useEffect } from "react";

export default function CourseCard(props) {
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);
  const [isOpen, setIsOpen] = useState(true);
  console.log(useState(0));
  let full = false;
  function enroll() {
    setCount(count + 1);
    setSeats(seats - 1);
  }
  useEffect(() => {
    if (seats === 0) {
      setIsOpen(false);
      alert("No more seats");
      document.querySelector("#button").setAttribute("disabled", true);
    }
  });
  return (
    <Col xs={12} md={6}>
      <Card className="cardHighlight p-3 m-2">
        <h3>{props.title}</h3>
        <p>Description: {props.description}</p>
        <p>Price: {Number(props.price).toLocaleString()}</p>
        <p>
          {/* Enrolled: {count}/30 ({30 - count} Remaining) */}
          Slots: {seats}
        </p>

        <Button id="button" onClick={enroll} variant="primary">
          Enroll now!
        </Button>
        <Button
          onClick={() => setCount(0)}
          variant="danger"
          hidden={!(count >= 30)}
        >
          Reset
        </Button>
      </Card>
    </Col>
  );
}

/* <Card.Body>
  <Card.Title></Card.Title>
</Card.Body>; */
