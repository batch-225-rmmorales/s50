// import "./App.css";
import "./App.css";
import CollapsibleExample from "./components/CollapsibleExample";
import Banner from "./components/Banner";
import Highlights from "./components/Highlights";
import { Container, Row } from "react-bootstrap";
import CourseCard from "./components/CourseCard";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Register from "./pages/Register";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import { useState, useEffect } from "react";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(
    localStorage.getItem("jwtToken") == 1
  );
  console.log("jwttoken is");
  console.log(localStorage.getItem("jwtToken"));
  console.log("isLoggedin is");
  console.log(isLoggedIn);
  return (
    <>
      <Router>
        <Routes>
          <Route
            path="/"
            element={
              <Container>
                <CollapsibleExample
                  isLoggedIn={isLoggedIn}
                  setIsLoggedIn={setIsLoggedIn}
                />
                <Home />
              </Container>
            }
          />
          <Route
            path="/register"
            element={
              <Container>
                <CollapsibleExample
                  isLoggedIn={isLoggedIn}
                  setIsLoggedIn={setIsLoggedIn}
                />
                <Register />
              </Container>
            }
          />
          <Route
            path="/login"
            element={
              <Container>
                <CollapsibleExample
                  isLoggedIn={isLoggedIn}
                  setIsLoggedIn={setIsLoggedIn}
                />
                <Login isLoggedIn={isLoggedIn} setIsLoggedIn={setIsLoggedIn} />
              </Container>
            }
          />
          <Route
            path="/courses"
            element={
              <Container>
                <CollapsibleExample
                  isLoggedIn={isLoggedIn}
                  setIsLoggedIn={setIsLoggedIn}
                />
                <Courses />
              </Container>
            }
          />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
